<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php
        require "./inc/css.inc";
        ?>
        <title>Ejercicio2</title>
    </head>
    <body>
        <?php require "./inc/menu.inc"; ?>
        <div class="my-4 container-fluid">
            <?php
            if (isset($_GET["ejercicio2"])) {
                require './inc/resultados2.inc';
            } else {
                require './inc/formulario2.inc';
            }
            ?>
        </div>
        <?php
        require './inc/js.inc';
        ?>
    </body>
</html>