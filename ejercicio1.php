<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">

        <!-- Bootstrap CSS -->
        <?php require "./inc/css.inc"; ?>
        <title>Ejercicio 1</title>
    </head>
    <body>
        <?php require "./inc/menu.inc"; ?>
        <div class="my-4 container-fluid">
            <?php
            if (isset($_GET["ejercicio1"])) {
                require './inc/resultados1.inc';
            } else {
                require './inc/formulario1.inc';
            }
            ?>
        </div>
        <?php
        require './inc/js.inc';
        ?>
    </body>
</html>