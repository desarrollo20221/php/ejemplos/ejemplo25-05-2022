<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php
        require "./inc/css.inc";
        ?>
        <title>Hello, world!</title>
    </head>
    <body>
        <?php
        require "./inc/menu.inc";
        ?>

        <?php
        require './inc/js.inc';
        ?>
    </body>
</html>