<!doctype html>
<html lang="en">
    <head>
        <!-- Required meta tags -->
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
        <?php require "./inc/css.inc"; ?>
        <title>Galeria 1</title>
    </head>
    <body>
        <?php require "./inc/menu.inc"; ?>
        <?php
        $galerias = [
            [
                [
                    "titulo" => "Foto 1",
                    "texto" => "lorem ipsum",
                    "src" => "./imgs/f1.jpg",
                    "fecha" => "1/1/2022"
                ],
                [
                    "titulo" => "Foto 2",
                    "texto" => "lorem ipsum",
                    "src" => "./imgs/f2.jpg",
                    "fecha" => "1/1/2022"
                ],
                [
                    "titulo" => "Foto 3",
                    "texto" => "lorem ipsum",
                    "src" => "./imgs/f3.jpg",
                    "fecha" => "1/1/2022"
                ],
                [
                    "titulo" => "Foto 4",
                    "texto" => "lorem ipsum",
                    "src" => "./imgs/f4.jpg",
                    "fecha" => "1/1/2022"
                ]
            ],
            [
                [
                    "titulo" => "Foto 5",
                    "texto" => "lorem ipsum",
                    "src" => "./imgs/f5.jpg",
                    "fecha" => "1/1/2022"
                ],
                [
                    "titulo" => "Foto 6",
                    "texto" => "lorem ipsum",
                    "src" => "./imgs/f6.jpg",
                    "fecha" => "1/1/2022"
                ],
                [
                    "titulo" => "Foto 7",
                    "texto" => "lorem ipsum",
                    "src" => "./imgs/f7.jpg",
                    "fecha" => "1/1/2022"
                ],
                [
                    "titulo" => "Foto 8",
                    "texto" => "lorem ipsum",
                    "src" => "./imgs/f8.jpg",
                    "fecha" => "1/1/2022"
                ]
            ]
        ];
        $numero = 0;
        require "./inc/galeria.inc";
        ?>
        <?php require "./inc/js.inc"; ?>
    </body>
</html>