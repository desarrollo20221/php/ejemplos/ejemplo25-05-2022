<form>
    <div class="form-group row">
        <label for="correo" class="col-lg-2 col-form-label">Email</label>
        <div class="col-lg-10">
            <input type="email" class="form-control" id="correo" placeholder="Email" name="correo" required>
        </div>
    </div>
    <div class="form-group row">
        <label for="password" class="col-lg-2 col-form-label">Password</label>
        <div class="col-lg-10">
            <input type="password" class="form-control" id="password" placeholder="contraseña" name="password" required>
        </div>
    </div>
    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-lg-2 pt-0">Mes de acceso</legend>
            <div class="col-lg-10">
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="mes" id="enero" value="enero" checked>
                    <label class="form-check-label" for="enero">
                        Enero
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="mes" id="febrero" value="febrero">
                    <label class="form-check-label" for="febrero">
                        Febrero
                    </label>
                </div>
                <div class="form-check">
                    <input class="form-check-input" type="radio" name="mes" id="marzo" value="marzo">
                    <label class="form-check-label" for="marzo">
                        Marzo
                    </label>
                </div>
            </div>
        </div>
    </fieldset>
    <fieldset class="form-group">
        <div class="row">
            <legend class="col-form-label col-lg-2 pt-0">Formas de acceso</legend>
            <div class="col-lg-10">
                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="formas[]" id="telefono" value="telefono" checked>
                    <label class="form-check-label" for="telefono">
                        Telefono
                    </label>
                </div>

                <div class="form-check">
                    <input class="form-check-input" type="checkbox" name="formas[]" id="ordenador" value="ordenador">
                    <label class="form-check-label" for="ordenador">
                        Ordenador
                    </label>
                </div>
            </div>
        </div>
    </fieldset>

    <div class="form-group row">
        <label for="ciudad" class="col-lg-2 col-form-label">Ciudad</label>
        <div class="col-lg-10">
            <select id="ciudad" class="form-control" name="ciudad" required>
                <option value="" disabled selected>Selecciona una ciudad</option>
                <option value="Santander">Santander</option>
                <option value="Laredo">Laredo</option>
            </select>
        </div>
    </div>

    <div class="form-group row">
        <label for="navegador" class="col-lg-2 col-form-label">Navegadores Utilizados</label>
        <div class="col-lg-10">
            <select id="navegador" class="form-control" name="navegador[]" multiple>
                <option value="" disabled selected>Selecciona navegadores</option>
                <option value="Google">Google</option>
                <option value="Edge">Edge</option>
                <option value="Safari">Safari</option>
            </select>
        </div>
    </div>
    <div class="form-group row">
        <div class="col-sm-10">
            <button name="ejercicio2" class="btn btn-primary">Enviar</button>
        </div>
    </div>
</form>
